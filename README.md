# microcutting

Comparison of micro-waterjet, laser, EDM, NC, ... cutting

## micro-waterjet
## [MicroMAX at OMAX Demo Lab](Omax/MicroMAX)


## solid-state laser micromachining
<!-- (picture of machine, cost of machine, machining time, picture before and after post-processing) -->

## ultrafast laser micromachining

## [EDM](Sodick/README.md)

## [Micromachining using Zund G-3 L-2500](Zund/README.md)

## [Micromachining using Datron Neo](Datron/)

## [Fablight fiber laser](FabLight/README.md)

