## Micromachining using Zund G-3 L-2500

Despite being a very large-scale tool, the Zund was surprisingly effective at micromachining with its 50kRPM router spindle.  To fixture the stock, we first faced a sheet of aluminum to provide a rigid surface.  Then we applied PSA tape to both this surface and the underside of the stock.  We burnished the tape using a small stainless rod.  Then we applied CA glue to the tape and bonded the stock to the substrate.

Using a .030" diameter end mill with amorphous diamond coating (<a href='http://www.harveytool.com/ToolTechInfo.aspx?ToolNumber=72030-C4'>Harvey Tool 72030-C4</a>), we machined the flexure from .020" Aluminum 2024 sheet in 2.5 minutes (comparable to the waterjet).  300um step down, 20 mm/s, 50kRPM.

Here is a 50% scale flexure (.010" beams, .015" gaps) machined from .020" thick Aluminum 2024 sheet using a .015" diameter end mill with amorphous diamond coating (<a href='http://www.harveytool.com/ToolTechInfo.aspx?ToolNumber=72015-C4'>Harvey Tool 72015-C4</a>).  This took about 8 minutes, but I think could be run faster.

<img src='flexure-0.5-penny.jpg' width=300px>

Here is a paper about micromachining aluminum using tools with diamond-like coatings: <a href='https://pdfs.semanticscholar.org/a9d5/532adaf5fa1b22940a921fc9cdf2ea76b555.pdf'>Diamond coatings for micro end mills: Enabling the dry machining of aluminum at the micro-scale, Heaney et. al.</a>

Harvey tools makes a .010" DLC tool that we should try for a 3x shrink of the original flexure.  They also make the same endmill line down to .001", but uncoated, which might be nice to try for less sticky metals than aluminum.  This would probably want a higher spindle speed yet, and more careful motion, so adding a spindle speeder to the Hurco VM10U could be an option to explore.  Here's one for 120kRPM that fits CAT40: https://www.bigkaiser.com/en/products/angle-heads-speed-increasers/40000-rpm/air-power-spindle-rbx12-rbx12c