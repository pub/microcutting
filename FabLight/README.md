# FabLight

http://cba.mit.edu/tools/display/?type=tool&id=fablight_3000_laser_cutter

Machining time: ~5 min

Cost of machine: sheet only machine with the 3000 laser is around $80,000, and the tube & sheet is around $90,000

Machine settings:

![Fablight settings](Fablight_settings.jpg)

Sample photos:

![fablight 1](Fablight_01.jpg)

![fablight 3](Fablight_03.JPG)

![fablight 5](Fablight_05.JPG)

After burr removal 

![fablight 6](Fablight_06.jpg)

The brown seen on the edges of the piece are burn marks where the machine cut. 

![fablight 7](Fablight_7.JPG)

![fablight 8](Fablight_08.JPG)
