# Omax MicroMax

https://www.omax.com/omax-waterjet/micromax
Nozzle: 7/15 MAXJET5 Minijet Nozzle with 0.007" ID diamond orifice and 0.015" Roctec 500 mixing tube

Abrasive: 240 mesh Barton HPX garnet with a mean particle size of 60 um at a flow rate of 0.12 lb/min

Pressure: 50 ~ 55 ksi

Cutting time: 3.1 min
